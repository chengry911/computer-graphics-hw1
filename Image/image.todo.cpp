#include "image.h"
#include <stdlib.h>
#include <math.h>
#define PI 3.141592653589793

////////////////////////////
// Image processing stuff //
////////////////////////////
Pixel::Pixel(const Pixel32& p)
{
}
Pixel32::Pixel32(const Pixel& p)
{
}


int Image32::AddRandomNoise(const float& noise,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input noise is %f.\n", noise);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	if (noise >= 0 && noise <= 1)
	{
		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				int rd = rand()%100;

				if(rd<noise*100){
				    p1.r = rand()%255;
				    p1.g = rand()%255;
				    p1.b = rand()%255;
				}else{
				    p1.r = pixels[ y*w + x].r;
					p1.g = pixels[ y*w + x].g;
					p1.b = pixels[ y*w + x].b;
				}
			}
		}
 
		return 1;
	}
	puts("Noise should be between 0 and 1.\n");
	return 0;
}
int Image32::Brighten(const float& brightness,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input brightness is %f.\n", brightness);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	double pr = 0.0;
	double pg = 0.0;
	double pb = 0.0;

	if (brightness >= 0)
	{
		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				pr = pixels[ y*w + x].r*brightness;
				pg = pixels[ y*w + x].g*brightness;
				pb = pixels[ y*w + x].b*brightness;

				if(pr<=255){
				    p1.r = pr;
				}else{
					p1.r = 255;
				}
				if(pg<=255){
				    p1.g = pg;
				}else{
				    p1.g = 255;
				}
				if(pb<=255){
					p1.b = pb;
				}else{
				    p1.b = 255;
				}
					
			}
		}
 
		return 1;
	}
	
	puts("Brightness should be larger than 0.\n");
	return 0;
}

int Image32::Luminance(Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double lum = 0.0;

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				lum = 0.30*pixels[ y*w + x].r + 0.59*pixels[ y*w + x].g + 0.11*pixels[ y*w + x].b;

				p1.r = lum;
				p1.g = lum;
				p1.b = lum;

			}
		}
 
		return 1;

}

int Image32::Contrast(const float& contrast,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input contrast is %f.\n", contrast);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double lum = 0.0;
	double avelum = 0.0;
	double pr = 0.0;
	double pg = 0.0;
	double pb = 0.0;

	if(contrast>0){
	    for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				avelum += 0.30*pixels[ y*w + x].r + 0.59*pixels[ y*w + x].g + 0.11*pixels[ y*w + x].b;

			}
		}
		
		avelum /=(w*h);
		printf("Average lum: %f.\n", avelum);

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				pr = (pixels[ y*w + x].r-avelum)*contrast + avelum;
				pg = (pixels[ y*w + x].g-avelum)*contrast + avelum;
				pb = (pixels[ y*w + x].b-avelum)*contrast + avelum;
				
				if(pr>=0 && pr<=255){
				    p1.r = pr;
				}else if(pr > 255){
					p1.r = 255;
				}else{
				    p1.r = 0;
				}
				if(pg>=0 && pg<=255){
				    p1.g = pg;
				}else if(pg > 255){
				    p1.g = 255;
				}else{
				    p1.g = 0;
				}
				if(pb>=0 && pb<=255){
					p1.b = pb;
				}else if(pb > 255){
				    p1.b = 255;
				}else{
				    p1.b = 0;
				}
			}
		}
 
		return 1;
	}
	puts("Contrast should be larger than 0.\n");
	return 0;

}

int Image32::Saturate(const float& saturation,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input contrast is %f.\n", saturation);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double lum = 0.0;
	double pr = 0.0;
	double pg = 0.0;
	double pb = 0.0;

	if(saturation>0){

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);

				lum = 0.30*pixels[ y*w + x].r + 0.59*pixels[ y*w + x].g + 0.11*pixels[ y*w + x].b;

				pr = (pixels[ y*w + x].r-lum)*saturation + lum;
				pg = (pixels[ y*w + x].g-lum)*saturation + lum;
				pb = (pixels[ y*w + x].b-lum)*saturation + lum;
				
				if(pr>=0 && pr<=255){
				    p1.r = pr;
				}else if(pr > 255){
					p1.r = 255;
				}else{
				    p1.r = 0;
				}
				if(pg>=0 && pg<=255){
				    p1.g = pg;
				}else if(pg > 255){
				    p1.g = 255;
				}else{
				    p1.g = 0;
				}
				if(pb>=0 && pb<=255){
					p1.b = pb;
				}else if(pb > 255){
				    p1.b = 255;
				}else{
				    p1.b = 0;
				}
			}
		}
 
		return 1;
	}
	puts("Saturation should be larger than 0.\n");
	return 0;
}

int Image32::Quantize(const int& bits,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input bits is %f.\n", bits);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double lum = 0.0;
	double pr = 0.0;
	double pg = 0.0;
	double pb = 0.0;

	int i = 0;
	int j = 0;
	int k = 0;

	if(bits>0){
		int div = pow(2.0, bits);

		int seglen = 255/(div-1);
		printf("%d %d \n",seglen, div );

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				Pixel32& p1 = outputImage.pixel(x, y);

				pr = pixels[ y*w + x].r/255.0;
				pg = pixels[ y*w + x].g/255.0;
				pb = pixels[ y*w + x].b/255.0;

				i = (int)(pr/seglen);
				j = (int)(pg/seglen);
				k = (int)(pb/seglen);

				//printf("Take: %lf\n",pixels[ y*w + x].r );
				//printf("Original: %d %d %d \n", (int)pr/seglen, (int)pg/seglen, (int)pb/seglen);
				//printf("%d", (int)pr/seglen);
				if(bits > 0){
				    p1.r = (floor(pr*div)/div)*255.0;
					p1.g = (floor(pg*div)/div)*255.0;
					p1.b = (floor(pb*div)/div)*255.0;
				}
				else if(bits<0){
					p1.r = (abs(i*seglen-pr)<=abs((i+1)*(seglen)-pr)? i*seglen : (i+1)*(seglen));
				    p1.g = (abs(j*seglen-pg)<=abs((j+1)*(seglen)-pg)? j*seglen : (j+1)*(seglen));
				    p1.b = (abs(k*seglen-pb)<=abs((k+1)*(seglen)-pb)? k*seglen : (k+1)*(seglen));
				}else{
					if(pixels[ y*w + x].r>128){
					    p1.r = 255;
					}else{
					    p1.r = 0;
					}
					if(pixels[ y*w + x].g>128){
					    p1.g = 255;
					}else{
					    p1.g = 0;
					}
					if(pixels[ y*w + x].b>128){
					    p1.b = 255;
					}else{
					    p1.b = 0;
					}
				}
			}
		}
 
		return 1;
	}
	puts("Bits should be larger than 0.\n");
	return 0;
}

int getDomain(int bits, int value){
	int len = 256/(pow(2.0, bits));
	int amount = (int)(value/len);

	return ( abs(value-amount*len) < abs(value-(amount+1)*len) ? amount*len-1 : (amount+1)*len)-1;
	}

int Image32::RandomDither(const int& bits,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input bits is %d.\n", bits);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double covR = 0.0;
	double covG = 0.0;
	double covB = 0.0;

	double rateR = 0.0;
	double rateG = 0.0;
	double rateB = 0.0;

	int div = pow(2.0, bits);

	if (bits >= 0)
	{
		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{								
				Pixel32& p1 = outputImage.pixel(x, y);

				covR = pixels[ y*w + x].r/255.0;
				covG = pixels[ y*w + x].g/255.0;
				covB = pixels[ y*w + x].b/255.0;

				rateR = ((covR + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)));
				if(rateR < 0){
					rateR = 0;
				}else if(rateR >1){
					rateR = 1;
				}
				rateG = ((covG + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)));
				if(rateG < 0){
					rateG = 0;
				}else if(rateG >1){
					rateG = 1;
				}
				rateB = ((covB + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)));
				if(rateB < 0){
					rateB = 0;
				}else if(rateB >1){
					rateB = 1;
				}
				p1.r = rateR*255;//((covR + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)))*255;
				p1.g = rateG*255;//((covG + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)))*255;
				p1.b = rateB*255;//((covB + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1)))*255;

				//printf("%d ", p1.r);
				//printf("%lf ",p1.r );
				if(p1.r>255){
					p1.r = 255;
				}else if(p1.r <0){
					p1.r = 0;
				}
				if(p1.g>255){
					p1.g = 255;
				}else if(p1.g <0){
					p1.g = 0;
				}
				if(p1.b>255){
					p1.b = 255;
				}else if(p1.b <0){
					p1.b = 0;
				}

			}
		}
 
		return 1;
	}
	puts("Bits should be larger than 0.\n");
	return 0;
}
int Image32::OrderedDither2X2(const int& bits,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input bits is %f.\n", bits);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	const int DSize = 2;
	double div = pow(2.0, bits);
	double perc = 1.0/div;
	int colorlen = 255/(div-1);

	int i=0;
	int j=0;
	int color = 0;

	double Dither[DSize][DSize] = {{1, 3},{4, 2}}; 

	double covR = 0.0;
	double covG = 0.0;
	double covB = 0.0;

	double errorR = 0.0;
	double errorG = 0.0;
	double errorB = 0.0;

	printf("%lf", perc);
	if (bits >= 0)
	{
		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{				
				
				Pixel32& p1 = outputImage.pixel(x, y);
				
				i = x % DSize;
				j = y % DSize;

				covR = pixels[ y*w + x].r;
				covG = pixels[ y*w + x].g;
				covB = pixels[ y*w + x].b;

				errorR = covR-(pixels[ y*w + x].r/colorlen)*colorlen;
				errorG = covG-(pixels[ y*w + x].g/colorlen)*colorlen;
				errorB = covB-(pixels[ y*w + x].b/colorlen)*colorlen;

				//printf("errorR: %lf floor: %lf ", errorR, Dither[i][j]/5*colorlen);

				if(errorR > Dither[i][j]/5*colorlen){
					covR = floor(covR/colorlen)*colorlen+colorlen;				  
				}else{
				    covR = floor(covR/colorlen)*colorlen;
				}
				if(errorG > Dither[i][j]/5*colorlen){
					covG = floor(covG/colorlen)*colorlen+colorlen;				  
				}else{
				    covG = floor(covG/colorlen)*colorlen;
				}
				if(errorB > Dither[i][j]/5*colorlen){
					covB = floor(covB/colorlen)*colorlen+colorlen;				  
				}else{
				    covB = floor(covB/colorlen)*colorlen;
				}
				//printf("covR: %lf", covR);
				if(covR>255){
					p1.r = 255;
				}else if(covR < 0){
					p1.r = 0;
				}else{
					p1.r = covR;
				}
				//printf("p1.r: %d", p1.r);
				if(covG>255){
					p1.g = 255;
				}else if(covG < 0){
					p1.g = 0;
				}else{
					p1.g = covG;
				}
				if(covB>255){
					p1.b = 255;
				}else if(covB < 0){
					p1.b = 0;
				}else{
					p1.b = covB;
				}

			}
		}
 
		return 1;
	}
	puts("Bits should be larger than 0.\n");
	return 0;
}

int Image32::FloydSteinbergDither(const int& bits,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("The input bits is %d.\n", bits);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	double covR = 0.0;
	double covG = 0.0;
	double covB = 0.0;

	double qr, qg, qb;
	double er, eg, eb;
	
	int div = pow(2.0, bits);
	int colorlen = 255/(div-1);

	if (bits >= 0)
	{
		for (int x = 0; x < outputImage.width(); x++)
		{
			for (int y = 0; y < outputImage.height(); y++)
			{								
				Pixel32& p1 = outputImage.pixel(x, y);
				Pixel32 p2 = pixel(x, y);
				//Assign the old pixel
				covR = p2.r;
				covG = p2.g;
				covB = p2.b;
				//Quantilize- find the closest palette color
				qr = floor((covR+p1.r)/colorlen)*colorlen;
				qg = floor((covG+p1.g)/colorlen)*colorlen;
				qb = floor((covB+p1.b)/colorlen)*colorlen;
								
				//Calculate errors
				er = covR+p1.r-qr;
				eg = covG+p1.g-qg;
				eb = covB+p1.b-qb;
							
				//printf("quant: %lf", qr );
				//Do the weightings
				if( y<outputImage.height()-1){
					Pixel32& pi = outputImage.pixel(x, y+1);
					Pixel32 pi2 = outputImage.pixel(x, y+1);
				    pi.r = pi2.r + er*7/16;
					pi.g = pi2.g + eg*7/16;
					pi.b = pi2.b + eb*7/16;
					//printf("%lf ", pi2.r + er*7/16);
				}
				if( x<outputImage.height()-1 && y > 0){
					Pixel32& pii = outputImage.pixel(x+1, y-1);
					Pixel32 pii2 = outputImage.pixel(x+1, y-1);
				    pii.r = pii2.r + er*3/16;
					pii.g = pii2.g + eg*3/16;
					pii.b = pii2.b + eb*3/16;
				}
				if( x<outputImage.width()-1){
					Pixel32& piii = outputImage.pixel(x+1, y);
					Pixel32 piii2 = outputImage.pixel(x+1, y);
				    piii.r = piii2.r + er*5/16;
					piii.g = piii2.g + eg*5/16;
					piii.b = piii2.b + eb*5/16;					
				}
				if( x<outputImage.width()-1 && y < outputImage.height()-1){
					Pixel32& piiii = outputImage.pixel(x+1, y+1);
					Pixel32 piiii2 = outputImage.pixel(x+1, y+1);
				    piiii.r = piiii2.r + er*1/16;
					piiii.g = piiii2.g + eg*1/16;
					piiii.b = piiii2.b + eb*1/16;						
				}
				
				//printf("%lf ",((covR + (rand()%10>=5?1:-1)*(rand()/(2.0*RAND_MAX))/div)/(1.0+1.0/pow(2.0, bits+1))) );
				//Assign new pixel to the output board
				if(qr>255){
					p1.r = 255;
				}else if(qr <0){
					p1.r = 0;
				}else{
					p1.r = qr;
				}
				if(qg>255){
					p1.g = 255;
				}else if(qg <0){
					p1.g = 0;
				}else{
					p1.g = qg;
				}
				if(qb>255){
					p1.b = 255;
				}else if(qb <0){
					p1.b = 0;
				}else{
					p1.b = qb;
				}

			}
		}
 
		return 1;
	}
	puts("Bits should be larger than 0.\n");
	return 0;
}

int Image32::Blur3X3(Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	const int DSize = 3;

	int filter[DSize][DSize] = {{1, 2, 1}, 
								{2, 4, 2}, 
								{1, 2, 1}}; 

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{	
				Pixel32& p1 = outputImage.pixel(x, y);

				if(y==0 || y== outputImage.height()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;
				}else if(x==0 || x == outputImage.width()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;				    
				}else{
				
				    //printf("%d ", filter[0][1]);
				    p1.r = pixels[ (y-1)*w + (x-1)].r*filter[0][0]/16 + pixels[ (y-1)*w + x].r*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].r*filter[0][2]/16 + pixels[ y*w + (x-1)].r*filter[1][0]/16
						+ pixels[ y*w + x].r*filter[1][1]/16 + pixels[ y*w + x+1 ].r*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].r*filter[2][0]/16 + pixels[ (y+1)*w + x].r*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].r*filter[2][2]/16;
				    p1.g = pixels[ (y-1)*w + (x-1)].g*filter[0][0]/16 + pixels[ (y-1)*w + x].g*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].g*filter[0][2]/16 + pixels[ y*w + (x-1)].g*filter[1][0]/16
						+ pixels[ y*w + x].g*filter[1][1]/16 + pixels[ y*w + x+1 ].g*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].g*filter[2][0]/16 + pixels[ (y+1)*w + x].g*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].g*filter[2][2]/16;
				    p1.b = pixels[ (y-1)*w + (x-1)].b*filter[0][0]/16 + pixels[ (y-1)*w + x].b*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].b*filter[0][2]/16 + pixels[ y*w + (x-1)].b*filter[1][0]/16
						+ pixels[ y*w + x].b*filter[1][1]/16 + pixels[ y*w + x+1 ].b*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].b*filter[2][0]/16 + pixels[ (y+1)*w + x].b*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].b*filter[2][2]/16;		
					if(p1.r <0){
						p1.r = 0;
					}
					if(p1.g < 0){
						p1.g = 0;
					}
					if(p1.b < 0){
						p1.b = 0;
					}
					if(p1.r > 255){
						p1.r = 255;
					}
					if(p1.g > 255){
						p1.g = 255;
					}
					if(p1.b > 255){
						p1.b = 255;
					}
				}	
			}
		}
 
		return 1;
}

int Image32::EdgeDetect3X3(Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	const int DSize = 3;

	int filter[DSize][DSize] = {{-1, -1, -1}, 
								{-1, 8, -1}, 
								{-1, -1, -1}}; 

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{	
				Pixel32& p1 = outputImage.pixel(x, y);

				if(y==0 || y== outputImage.height()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;
				}else if(x==0 || x == outputImage.width()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;				    
				}else{
				
				    //printf("%d ", filter[0][1]);
				    p1.r = pixels[ (y-1)*w + (x-1)].r*filter[0][0]/16 + pixels[ (y-1)*w + x].r*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].r*filter[0][2]/16 + pixels[ y*w + (x-1)].r*filter[1][0]/16
						+ pixels[ y*w + x].r*filter[1][1]/16 + pixels[ y*w + x+1 ].r*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].r*filter[2][0]/16 + pixels[ (y+1)*w + x].r*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].r*filter[2][2]/16;
				    p1.g = pixels[ (y-1)*w + (x-1)].g*filter[0][0]/16 + pixels[ (y-1)*w + x].g*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].g*filter[0][2]/16 + pixels[ y*w + (x-1)].g*filter[1][0]/16
						+ pixels[ y*w + x].g*filter[1][1]/16 + pixels[ y*w + x+1 ].g*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].g*filter[2][0]/16 + pixels[ (y+1)*w + x].g*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].g*filter[2][2]/16;
				    p1.b = pixels[ (y-1)*w + (x-1)].b*filter[0][0]/16 + pixels[ (y-1)*w + x].b*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].b*filter[0][2]/16 + pixels[ y*w + (x-1)].b*filter[1][0]/16
						+ pixels[ y*w + x].b*filter[1][1]/16 + pixels[ y*w + x+1 ].b*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].b*filter[2][0]/16 + pixels[ (y+1)*w + x].b*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].b*filter[2][2]/16;		
					if(p1.r <0){
						p1.r = 0;
					}
					if(p1.g < 0){
						p1.g = 0;
					}
					if(p1.b < 0){
						p1.b = 0;
					}
					if(p1.r > 255){
						p1.r = 255;
					}
					if(p1.g > 255){
						p1.g = 255;
					}
					if(p1.b > 255){
						p1.b = 255;
					}
				}	
			}
		}
 
		return 1;
}
int Image32::ScaleNearest(const float& scaleFactor,Image32& outputImage) const
{
	int ow, oh;
	double ix, iy;
	ow = w*scaleFactor;
	oh = h*scaleFactor;
	outputImage.setSize(ow,oh);
	if(scaleFactor >0){
		for(int y=0; y<oh; y++){
			for(int x=0; x<ow; x++){
				ix = x/scaleFactor;
				iy = y/scaleFactor;
				Pixel32& p1 = outputImage.pixel(x, y);
				Pixel32 p2 = NearestSample(ix, iy);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
			}
		}

		return 1;
	}else{
		return 0;
	}
}

int Image32::ScaleBilinear(const float& scaleFactor,Image32& outputImage) const
{
	int ow, oh;
	double ix, iy;
	ow = w*scaleFactor;
	oh = h*scaleFactor;
	outputImage.setSize(ow,oh);
	if(scaleFactor >0){
		for(int y=0; y<oh; y++){
			for(int x=0; x<ow; x++){
				ix = x/scaleFactor;
				iy = y/scaleFactor;
				Pixel32& p1 = outputImage.pixel(x, y);
				Pixel32 p2 = BilinearSample(ix, iy);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
				//printf("pixel: %d ",p1.r);
			}
		}

		return 1;
	}else{
		return 0;
	}
}

int Image32::ScaleGaussian(const float& scaleFactor,Image32& outputImage) const
{
	int ow, oh;
	double ix, iy;
	ow = w*scaleFactor;
	oh = h*scaleFactor;
	outputImage.setSize(ow,oh);
	if(scaleFactor >0){
		for(int y=0; y<oh; y++){
			for(int x=0; x<ow; x++){
				ix = x/scaleFactor;
				iy = y/scaleFactor;
				Pixel32& p1 = outputImage.pixel(x, y);
				Pixel32 p2 = GaussianSample(ix, iy, 0.2, 2.2);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
				//printf("pixel: %d ",p2.r);
			}
		}

		return 1;
	}else{
		return 0;
	}
}

int Image32::RotateNearest(const float& angle,Image32& outputImage) const
{
	double radian = PI*(angle/180.0);
    //int ow = w*2;
	//int oh = h*2;
	double ix, iy;
	double u = 0.0;
	double v = 0.0;

	//The location of the pixel at left top, right top, left bottom, right bottom
	double pltx = -w/2.0; double plty = h/2.0;
	double prtx = w/2.0; double prty = h/2.0;
	double plbx = -w/2.0; double plby = -h/2.0;
	double prbx = w/2.0; double prby = -h/2.0;

	//the sin cos calculation
	double sina = sin(radian);
	double cosa = cos(radian);

	//Calculate the location of corners after rotation
	double pltnx = pltx*cosa + plty*sina;
	double pltny = -pltx*sina + plty*cosa;
	double prtnx = prtx*cosa + prty*sina;
	double prtny = -prtx*sina + prty*cosa;
	double plbnx = plbx*cosa + plby*sina;
	double plbny = -plbx*sina + plby*cosa;
	double prbnx = prbx*cosa + prby*sina;
	double prbny = -prbx*sina + prby*cosa;

	//The size after rotation
	int destiWidth = (abs(prbnx-pltnx)> abs(prtnx - plbnx)? abs(prbnx-pltnx): abs(prtnx - plbnx));
	int destiHeight = (abs(prbny-pltny)>abs(prtny-plbny) ? abs(prbny-pltny): abs(prtny-plbny));
	printf("A: %lf B: %lf", abs(prbny-pltny),abs(prtny-plbny) );
    outputImage.setSize(destiWidth,destiHeight);
	for(int y=0; y<destiHeight; y++){
		for(int x=0; x<destiWidth; x++){
			Pixel32& p1 = outputImage.pixel(x, y);
			//Based on the center of the image, rotate
			u = (x-destiWidth/2)*cos(2*PI-radian) + (-y + destiHeight/2)*sin(2*PI-radian);
			v = -(x- destiWidth/2)*sin(2*PI-radian) + (-y + destiHeight/2)*cos(2*PI-radian);
			//If the pixel is not in the original image, black it.
			if( u > w/2 || u < -w/2 || v > h/2 || v < -h/2){
				p1.r = 0;
				p1.g = 0;
				p1.b = 0;
			}else{
				int ou = u + w/2;
				int ov = abs(v - h/2);
				if( ou < 0){
					ou = 0;
				}else if( ou > w-1 ){
					ou = w-1;
				}
				if( ov < 0){
					ov = 0;
				}else if( ov > h-1 ){
					ov = h-1;
				}
				Pixel32 p2 = NearestSample(ou, ov);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
			}
		}
	}
	return 1;
}

int Image32::RotateBilinear(const float& angle,Image32& outputImage) const
{
	double radian = PI*(angle/180.0);
    //int ow = w*2;
	//int oh = h*2;
	double ix, iy;
	double u = 0.0;
	double v = 0.0;

	//The location of the pixel at left top, right top, left bottom, right bottom
	double pltx = -w/2.0; double plty = h/2.0;
	double prtx = w/2.0; double prty = h/2.0;
	double plbx = -w/2.0; double plby = -h/2.0;
	double prbx = w/2.0; double prby = -h/2.0;

	//the sin cos calculation
	double sina = sin(radian);
	double cosa = cos(radian);

	//Calculate the location of corners after rotation
	double pltnx = pltx*cosa + plty*sina;
	double pltny = -pltx*sina + plty*cosa;
	double prtnx = prtx*cosa + prty*sina;
	double prtny = -prtx*sina + prty*cosa;
	double plbnx = plbx*cosa + plby*sina;
	double plbny = -plbx*sina + plby*cosa;
	double prbnx = prbx*cosa + prby*sina;
	double prbny = -prbx*sina + prby*cosa;

	//The size after rotation
	int destiWidth = (abs(prbnx-pltnx)> abs(prtnx - plbnx)? abs(prbnx-pltnx): abs(prtnx - plbnx));
	int destiHeight = (abs(prbny-pltny)>abs(prtny-plbny) ? abs(prbny-pltny): abs(prtny-plbny));
	printf("A: %lf B: %lf", abs(prbny-pltny),abs(prtny-plbny) );
    outputImage.setSize(destiWidth,destiHeight);
	for(int y=0; y<destiHeight; y++){
		for(int x=0; x<destiWidth; x++){
			Pixel32& p1 = outputImage.pixel(x, y);
			//Based on the center of the image, rotate
			u = (x-destiWidth/2)*cos(2*PI-radian) + (-y + destiHeight/2)*sin(2*PI-radian);
			v = -(x- destiWidth/2)*sin(2*PI-radian) + (-y + destiHeight/2)*cos(2*PI-radian);
			//If the pixel is not in the original image, black it.
			if( u > w/2 || u < -w/2 || v > h/2 || v < -h/2){
				p1.r = 0;
				p1.g = 0;
				p1.b = 0;
			}else{
				int ou = u + w/2;
				int ov = abs(v - h/2);
				if( ou < 0){
					ou = 0;
				}else if( ou > w-1 ){
					ou = w-1;
				}
				if( ov < 0){
					ov = 0;
				}else if( ov > h-1 ){
					ov = h-1;
				}
				Pixel32 p2 = BilinearSample(ou, ov);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
			}
		}
	}
	return 1;
}
	
int Image32::RotateGaussian(const float& angle,Image32& outputImage) const 
{
	double radian = PI*(angle/180.0);
    //int ow = w*2;
	//int oh = h*2;
	double ix, iy;
	double u = 0.0;
	double v = 0.0;

	//The location of the pixel at left top, right top, left bottom, right bottom
	double pltx = -w/2.0; double plty = h/2.0;
	double prtx = w/2.0; double prty = h/2.0;
	double plbx = -w/2.0; double plby = -h/2.0;
	double prbx = w/2.0; double prby = -h/2.0;

	//the sin cos calculation
	double sina = sin(radian);
	double cosa = cos(radian);

	//Calculate the location of corners after rotation
	double pltnx = pltx*cosa + plty*sina;
	double pltny = -pltx*sina + plty*cosa;
	double prtnx = prtx*cosa + prty*sina;
	double prtny = -prtx*sina + prty*cosa;
	double plbnx = plbx*cosa + plby*sina;
	double plbny = -plbx*sina + plby*cosa;
	double prbnx = prbx*cosa + prby*sina;
	double prbny = -prbx*sina + prby*cosa;

	//The size after rotation
	int destiWidth = (abs(prbnx-pltnx)> abs(prtnx - plbnx)? abs(prbnx-pltnx): abs(prtnx - plbnx));
	int destiHeight = (abs(prbny-pltny)>abs(prtny-plbny) ? abs(prbny-pltny): abs(prtny-plbny));
	//printf("A: %lf B: %lf", abs(prbny-pltny),abs(prtny-plbny) );
    outputImage.setSize(destiWidth,destiHeight);
	for(int y=0; y<destiHeight; y++){
		for(int x=0; x<destiWidth; x++){
			Pixel32& p1 = outputImage.pixel(x, y);
			//Based on the center of the image, rotate
			u = (x-destiWidth/2)*cos(2*PI-radian) + (-y + destiHeight/2)*sin(2*PI-radian);
			v = -(x- destiWidth/2)*sin(2*PI-radian) + (-y + destiHeight/2)*cos(2*PI-radian);
			//If the pixel is not in the original image, black it.
			if( u > w/2 || u < -w/2 || v > h/2 || v < -h/2){
				p1.r = 0;
				p1.g = 0;
				p1.b = 0;
			}else{
				int ou = u + w/2;
				int ov = abs(v - h/2);
				if( ou < 0){
					ou = 0;
				}else if( ou > w-1 ){
					ou = w-1;
				}
				if( ov < 0){
					ov = 0;
				}else if( ov > h-1 ){
					ov = h-1;
				}
				Pixel32 p2 = GaussianSample(ou, ov, 0.2, 2.2);
				p1.r = p2.r;
				p1.g = p2.g;
				p1.b = p2.b;
			}
		}
	}
	return 1;
}


int Image32::SetAlpha(const Image32& matte)
{
	for(int x=0; x<w; x++){
		for(int y=0; y<h; y++){
			Pixel32 mp = matte.pixel(x,y);
			//printf("%d ", mp.b);
			if(mp.b == 255){
				pixel(x, y).a = 0;
								
			}else if(mp.b == 0){
				pixel(x, y).a = 255;
				
			}else{
				pixel(x, y).a = 255-mp.b;
				
			}
		}
	}
	return 1;
}

int Image32::Composite(const Image32& overlay,Image32& outputImage) const
{
	outputImage.setSize(w,h);
	double overlayA = 0.0;
	double inputA = 0.0;

	for(int x=0; x<w; x++){
		for(int y=0; y<h; y++){
			
			Pixel32& p1 = outputImage.pixel(x, y);
			Pixel32 p2 = overlay.pixel(x, y);
			overlayA = p2.a/255.0;
			inputA = pixel(x,y).a/255.0;

			p1.r = (overlayA*p2.r) + ((1-overlayA)*inputA*pixel(x,y).r);
			p1.g = (overlayA*p2.g) + ((1-overlayA)*inputA*pixel(x,y).g);
			p1.b = (overlayA*p2.b) + ((1-overlayA)*inputA*pixel(x,y).b);

		}
	}
	return 1;
}

int Image32::CrossDissolve(const Image32& source,const Image32& destination,const float& blendWeight,Image32& outputImage)
{
	//new rgb coordinates
	double nr, ng, nb;
	outputImage.setSize(destination.width(), destination.height());
	printf("%d %d ", outputImage.width(), outputImage.height());
	for(int x=0; x<outputImage.width(); x++){
		for(int y=0; y<outputImage.height(); y++){
			
			nr = source.pixel(x,y).r*blendWeight + destination.pixel(x,y).r*blendWeight;
			ng = source.pixel(x,y).g*blendWeight + destination.pixel(x,y).g*blendWeight;
			nb = source.pixel(x,y).b*blendWeight + destination.pixel(x,y).b*blendWeight;
			outputImage.pixel(x, y).r = nr;
			outputImage.pixel(x, y).g = ng;
			outputImage.pixel(x, y).b = nb;
		}
	}
	
	return 1;
}
int Image32::Warp(const OrientedLineSegmentPairs& olsp,Image32& outputImage) const
{
	outputImage.setSize(w, h);
	//printf("%d %d ", w, h);
	float sx = 0.0;
	float sy = 0.0;
	/**
	//Displacement of coordinates
	float displacemtX = 0.0;
	float displacemtY = 0.0;

				//Total displacement
			float dsumX = 0.0;
			float dsumY = 0.0;

			float weightsum = 0.0;
			**/
	for(int x=0; x<w; x++){
		for(int y=0; y<h; y++){


			Pixel32& p1 = outputImage.pixel(x, y);
			//printf("%d ", y);

			olsp.getSourcePosition(x, y, sx, sy);
			
		    if( sx < 0){
				sx = 0;
			}else if( sx > w-1 ){
				sx = w-1;
			}
			if( sy < 0){
				sy = 0;
			}else if( sy > h-1 ){
				sy = h-1;
			}
			
			//displacemtX = sx - x;
			//displacemtY = sy - y;
			//printf("sx: %f %f ", sx, sy);
			p1 = GaussianSample(sx, sy, 0.2, 2.2);
			//printf("%d %d %d ", p1.r, p1.g, p1.b);
		}
	}
	return 1;
}

int Image32::FunFilter(Image32& outputImage) const
{
	outputImage.setSize(w, h);

	float angle = 0.0;
	float rotation = 0.0;
	//Center position
	float cx = w/2.0;
	float cy = w/2.0;
	//Coordinations based on center
	float i = 0.0;
	float j = 0.0;
	//Original coordinates
	float u = 0.0;
	float v = 0.0;

	//Degree of rotation
	float degree = 0.006;
	for(int y=0; y<h; y++){
		for(int x=0; x<w; x++){
			Pixel32& p1 = outputImage.pixel(x, y);

			if( 0.3*w< x < 0.7*w && 0.3*h< y < 0.7*h){
				i = x-cx;
				j = y-cy;
				angle = atan2(j, i);
				rotation = sqrt(i*i + j*j);

				u = rotation*cos(2*angle + (degree*rotation)) + cx;
				v = rotation*sin(2*angle + (degree*rotation)) + cy;
				if( u < 0){
					u = 0;
				}else if( u > w-1 ){
					u = w-1;
				}
				if( v < 0){
					v = 0;
				}else if( v > h-1 ){
					v = h-1;
				}
				
				p1 = GaussianSample(u, v, 0.2, 2.2);
			}else{
				p1 = GaussianSample(x, y, 0.2, 2.2);
			}
			
		}
	}

	return 1;
}
int Image32::Crop(const int& x1,const int& y1,const int& x2,const int& y2,Image32& outputImage) const
{
	if( x1>w-1 || x1 < 0 || x2 > w-1 || x2 < 0 || y1 > h-1 || y1 < 0 || y2 > h-1 || y2 < 0){
		printf("Pixels out of range, please set within %d x %d.\n", w, h);
		return 0;
	}else{
		//Output width and output height
		int ow = abs(x2 - x1);
		int oh = abs(y2 - y1);
		outputImage.setSize(ow, oh);

		int startx = (x1<x2?x1:x2);
		int scalex = abs(x2-x1);
		int starty = (y1<y2?y1:y2);
		int scaley = abs(y2-y1);
		int newx = 0;
		int newy = 0;

		for (int x=0; x<ow; x++){
			for(int y=0; y<oh; y++){
				Pixel32& p1 = outputImage.pixel(x, y);
				newx = x+startx;
				newy = y+starty;
				p1 = pixel(newx, newy);
			}
		}
		return 1;
	}
	return 1;
}


Pixel32 Image32::NearestSample(const float& x,const float& y) const
{
	int i = int(x+0.5);
	int j = int(y+0.5);
	Pixel32 p = pixel(i, j);
	
	return p;
}
Pixel32 Image32::BilinearSample(const float& x,const float& y) const
{
	double x1 = floor(x);
	double x2 = x1 + 1.0;
	double y1 = floor(y);
	double y2 = y1 + 1.0;
	double dx = x - x1;
	double dy = y - y1;
	double r = 0.0;
	double g = 0.0;
	double b = 0.0;
	//int ox = int(x1*(1-dx) + x2*dx);
	//int oy = int(y1*(1-dy) + y2*dy);
	
	Pixel32 p = pixel(x,y);
	if(x1<0){
		x1 = 0;
	}else if(x1 > w-1){
		x1 = w-1;
	}
	if(x2 < 0){
		x2 = 0;
	}else if(x2 > w-1){
		x2 = w-1;
	}
	if(y1 < 0){
		y1 = 0;
	}else if(y1 > h-1){
		y1 = h-1;
	}
	if(y2 < 0){
		y2 = 0;
	}else if(y2 > h-1){
		y2 = h-1;
	}
	r = double(pixel(x1, y1).r)*(1-dx) + double(pixel(x2, y2).r)*dx;
	g = double(pixel(x1, y1).g)*(1-dx) + double(pixel(x2, y2).g)*dx;
	b = double(pixel(x1, y1).b)*(1-dx) + double(pixel(x2, y2).b)*dx;
	p.r = int(r);
	p.g = int(g);
	p.b = int(b);
	//printf("%lf ",p.r);
	return p;
}

Pixel32 Image32::GaussianSample(const float& x,const float& y,const float& variance,const float& radius) const
{
	//int x1 = 1.1;
	//double result;
	//result = (1/sqrt(variance*2*3.1415926))*exp(-1.0*x1*x1/(2.0*variance));

	Pixel32 p = pixel(x, y);
	double upperx=0.0;
	double lowerx=0.0;
	double uppery=0.0;
	double lowery=0.0;
	double sumr=0.0;
	double sumg=0.0;
	double sumb=0.0;
	double sum = 0.0;
	double dist = 0.0;
	upperx = x + radius;
	lowerx = x - radius;
	uppery = y + radius;
	lowery = y - radius;
	if(upperx>w-1){
		upperx = w-1;
	}
	if(lowerx < 0){
		lowerx = 0;
	}
	if(uppery > h-1){
		uppery = h-1;
	}
	if(lowery < 0){
		lowery = 0;
	}
	//printf("x: %lf y: %lf lowerx: %lf upperx: %lf lowery: %lf uppery: %lf \n", x, y, lowerx, upperx, lowery, uppery);
	for(int m=int(ceil(lowerx)); m<=int(upperx); m++){
		for(int n=int(ceil(lowery)); n<=int(uppery); n++){
			dist = sqrt((m-x)*(m-x)+(n-y)*(n-y));
			sum +=(1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance));
		}
	}
	for(int m=int(ceil(lowerx)); m<=int(upperx); m++){
		for(int n=int(ceil(lowery)); n<=int(uppery); n++){
			dist = sqrt((m-x)*(m-x)+(n-y)*(n-y));
			sumr += double(pixel(m,n).r)*(1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance))/sum;
			sumg += double(pixel(m,n).g)*(1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance))/sum;
			sumb += double(pixel(m,n).b)*(1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance))/sum;
			//printf("pixel: %d ",double(pixel(m,n).r)*(1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance)));
			//printf("%d ", pixel(m,n).r);
			//printf("x: %lf y: %lf lowerx: %lf upperx: %lf lowery: %lf uppery: %lf m: %d n: %d dist: %lf perc: %lf\n", x, y, lowerx, upperx, lowery, uppery, m, n, dist, (1/sqrt(variance*2*3.1415926))*exp(-1.0*dist*dist/(2.0*variance)));
		}
	}
	
	p.r = (int)sumr;
	p.g = (int)sumg;
	p.b = (int)sumb;
	
	//p = pixel((int)sumr, (int)sumg);
	return p;
}
