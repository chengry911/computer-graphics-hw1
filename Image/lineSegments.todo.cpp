#include "lineSegments.h"
#include <math.h>

////////////////////////////
// Image processing stuff //
////////////////////////////
float OrientedLineSegment::length(void) const
{
	//printf("%d %d %d %d ", x1, y1, x2, y2);
	double len = 0.0;
	double X = abs(x1-x2);
	double Y = abs(y1-y2);
	len = sqrt(X*X + Y*Y);
	return len;
}
float OrientedLineSegment::distance(const int& x,const int& y) const
{
	double len = length();
	double X1 = abs(x-x1);
	double Y1 = abs(y-y1);
	double X2 = abs(x-x2);
	double Y2 = abs(y-y2);

	double d1 = sqrt(X1*X1 + Y1*Y1);
	double d2 = sqrt(X2*X2 + Y2*Y2);

	double angle = atan2(d2, d1);
	
	double d3 = d1*cos(angle);

	if(d3 > len ) {
		return d2;
	}
	if (d3<0) {
		return d1;
	}else{
		return d1*sin(angle);
	}
	
}
void  OrientedLineSegment::getPerpendicular(float& x,float &y) const
{
	/** The coordinates of the vector perpendicular to PQ*/
	//printf("before: %f %f\n ", x, y);
	double vx = 0.0;
	double vy = 0.0;
	y+=0.0000001;
	vx = sqrt(1.0/(1.0+(pow(x,2)/pow(y,2))));
	//printf("half: %lf \n", pow(x,2)/pow(y,2));
	vy = -1*(x*vx/y);
	x = vx;
	y = vy;
	//printf("after x: %f y: %f\n", x, y);
}

void  OrientedLineSegment::GetSourcePosition(const OrientedLineSegment& source,const OrientedLineSegment& destination,
											 const int& targetX,const int& targetY,
											 float& sourceX,float& sourceY)
{
	//destination PQ scalar
	int dPQx = destination.x2 - destination.x1;
	int dPQy = destination.y2 - destination.y1;
	//printf("dPQx before: %d %d ", dPQx, dPQy);
	//source PQ scalar
	int sPQx = source.x2 - source.x1;
	int sPQy = source.y2 - source.y1;

	//destination PX scalar
	int dPXx = targetX - destination.x1;
	int dPXy = targetY - destination.y1;
	//printf("source: %d %d %d %d\n", source.x1, source.y1, source.x2, source.y2);
	//printf("desti: %d %d %d %d\n", destination.x1, destination.y1, destination.x2, destination.y2);
	//Perpendicular unit vector with regard to destination PQ
	float pdx = 0.0;
	float pdy = 0.0;
	pdx = dPQx;
	pdy = dPQy;
	//printf("dPQx before: %f %f \n", pdx, pdy);
	destination.getPerpendicular(pdx, pdy);
	//printf("dPQx after: %f %f \n", pdx, pdy);
	//printf("sPQx after: %d %d \n", sPQx, sPQy); 
	//Perpendicular unit vector with regard to source PQ
	float psx = 0.0;
	float psy = 0.0;
	psx = sPQx;
	psy = sPQy;
	destination.getPerpendicular(psx, psy);
	
	//the u value along the distence from the segment to point x
	double u = 0.0;
	u = double(dPXx*dPQx + dPXy*dPQy)/double(dPQx*dPQx + dPQy*dPQy);

	//the v value along the PQ scalar
	double v = 0.0;
	v = (dPXx*pdx + dPXy*pdy);

	//Source point X'
	sourceX = source.x1 + u*sPQx + v*psx;
	sourceY = source.y1 + u*sPQy + v*psy;

}
